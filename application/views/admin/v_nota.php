<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Nota</title>

	

		
		<link href="<?php echo base_url().'assets/style-material.css'?>" rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/materialadmin.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/material-design-iconic-font.min.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/jquery.dataTables.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/extensions/dataTables.colVis.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/extensions/dataTables.tableTools.css'?>" />
		

	</head>
	<body class="menubar-hoverable header-fixed ">


			
			<div id="content">
				<section>
					<div class="section-header">
							<h2><span class="fa fa-cart-arrow-down"></span> Nota</h2>
					</div>
						<?php echo $this->session->flashdata('msg');?>
				</section>


				


		<!--  EDIT PENGGUNA  -->
			<?php 
			
				foreach ($data->result_array() as $a) {
					$id=$a['inv_no'];
					$tanggal=$a['inv_tanggal'];
					$plg_id=$a['inv_plg_id'];
					$plg_nama=$a['inv_plg_nama'];
					$status=$a['inv_status'];
					$rek_id=$a['inv_rek_id'];
					$total=$a['inv_total'];
					$rek_bank=$a['inv_rek_bank'];
								
			?>
			       
			    </div>
			
			      
									<div class="form-group">
										<div class="col-sm-12">
											<table>
												<tr>
													<td style="width:120px;">Tanggal</td>
													<td>:</td>
													<td><?php echo $tanggal;?></td>
												</tr>
												<tr>
													<td>Pelanggan</td>
													<td>:</td>
													<td><?php echo $plg_nama;?></td>
												</tr>
												<tr>
													<td>Status Pesanan</td>
													<td>:</td>
													<td><?php echo $status;?></td>
												</tr>
												<?php if($rek_id=='COD'):?>
												<tr>
													<td>Pembayaran</td>
													<td>:</td>
													<td><?php echo $rek_id;?></td>
												</tr>
												<?php else:?>
												<tr>
													<td>Pembayaran</td>
													<td>:</td>
													<td><?php echo 'Transfer Bank '.$rek_bank;?></td>
												</tr>
												<?php endif;?>
											</table><br/>
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>Menu</th>
														<th style="text-align:center;">Harga</th>
														<th style="text-align:center;">Porsi</th>
														<th style="text-align:center;">Subtotal</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$query=$this->db->query("SELECT * FROM tbl_detail WHERE detail_inv_no='$id'  ");
														foreach ($query->result_array() as $j) {
															$menu_nama=$j['detail_menu_nama'];
															$menu_harjul=$j['detail_harjul'];
															$menu_porsi=$j['detail_porsi'];
															$menu_subtotal=$j['detail_subtotal'];
														
													?>
													<tr>
														<td><?php echo $menu_nama;?></td>
														<td style="text-align:center;"><?php echo number_format($menu_harjul);?></td>
														<td style="text-align:center;"><?php echo $menu_porsi;?></td>
														<td style="text-align:center;"><?php echo number_format($menu_subtotal);?></td>
													</tr>
													<?php } ?>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="3">Total</td>
														<td style="text-align:center;"><?php echo number_format($total);?></td>
													</tr>
												</tfoot>
												
											</table>
										</div>
									</div>
									
								
			        </div>

			    </div>
			    </div>
			</div>
			<?php } ?>


			<!--  EDIT PENGGUNA  -->
            <script type ="text/javascript">
									window.print();
									</script>

		
		<script src="<?php echo base_url().'assets/js/jquery/jquery-1.11.2.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery/jquery-migrate-1.2.1.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/bootstrap/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/spin/spin.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/autosize/jquery.autosize.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/jquery.dataTables.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/extensions/ColVis/js/dataTables.colVis.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/nanoscroller/jquery.nanoscroller.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/App.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavigation.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppOffcanvas.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppCard.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppForm.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavSearch.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppVendor.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/core/DemoTableDynamic.js'?>"></script>
		
		
	</body>
</html>
