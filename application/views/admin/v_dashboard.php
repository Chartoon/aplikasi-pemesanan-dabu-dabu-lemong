<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard</title>

		
		<link rel="shorcut icon" href="<?php echo base_url().'assets/img/logo.png'?>">
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/style-material.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/materialadmin.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/material-design-iconic-font.min.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/rickshaw.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/morris.core.css'?>" />
		
	</head>
	<body class="menubar-hoverable header-fixed ">

		<?php 
			$this->load->view('admin/v_header');
		?>

		<div id="base">

			<div class="offcanvas">
			</div>

			<div id="content">
				<section>
					<div class="section-body">
						<div class="row">
							<?php 
								$l=$pen_last->row_array();
							?>
							
							<div class="col-md-3 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">

									
											<div class="stick-bottom-left-right">
												<div class="height-5 sparkline-revenue" data-line-color="#bdc1c1"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<?php 
								$c=$pen_now->row_array();
							?>
							
							<div class="col-md-6 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">
									
										<div class="">
											<strong class="pull-right text-warning text-lg"> <i class=""></i></strong>
											<strong class="text-xl"><?php echo 'Rp '.number_format($c['total_penjualan']);?></strong><br/>
											<span class="opacity-100">Penjualan Bulan Ini</span>
											<div class="stick-bottom-right">
												<div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<?php 
								$p=$tot_porsi->row_array();
							?>
							
							<div class="col-md-6 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">
									
										<div class="">
											<strong class="pull-right text-danger text-lg"> <i class=""></i></strong>
											<strong class="text-xl"><?php echo $p['total_porsi'];?></strong><br/>
											<span class="opacity-100">Total Porsi Terjual Bulan Ini</span>
											<div class="stick-bottom-right">
												<div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<?php 
								$t=$tot_plg->row_array();
							?>
						
							<div class="col-md-3 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">
									
									
										</div>
									</div>
								</div>
							</div>
							

						</div>
						<div class="row">

						<?php
						  
						    error_reporting(0);
						    foreach($statistik as $result){
						        $bulan[] = $result->tanggal; 
						        $value[] = (float) $result->total; 
						    }
						   
						     
						?>

							
							<div class="col-md-12">
								<div class="card ">
									<div class="row">
										<div class="col-md-12">
											<div class="card-head">
												<header>Statistik Penjualan</header>
											</div>
											<div class="card-body height-12">
												<div class="flot-legend-horizontal stick-top-right no-y-padding">
													<canvas id="canvas" width="1200" height="300"></canvas>
												</div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							

							

						</div>
						<div class="row">

							
											<?php 
												foreach ($odr->result_array() as $o) {
													$oid=$o[''];
													$otgl=$o[''];
													$oplg=$o[''];
											?>
											<!--
											
															<?php echo $oid;?>
															<?php echo $otgl.'<br/>'.$oplg;?>
													<?php echo base_url().'admin/order'?>">
													
											<?php } ?>
									
							<?php
							   
							    error_reporting(0);
							    foreach($statistikplg as $result){
							        $bln[] = $result->bulan; 
							        $val[] = (float) $result->total; 
							    }
							  
						     
							?>
							 
							
						
										<?php 
											foreach ($plg->result_array() as $p) {
												$idpel=$p['plg_id'];
												$napel=$p['plg_nama'];
												$ptpel=$p['plg_photo'];
											
										?>
										
							
										<?php } ?>
											
										 -->

						</div>
					</div>
				</section>
			</div>
			

			<div id="menubar" class="menubar-inverse">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					
				</div>
				<div class="menubar-scroll-panel">

					
					<ul id="main-menu" class="gui-controls">

						
						<li>
							<a href="<?php echo base_url().'admin/dashboard'?>" class="active">
								<div class="gui-icon"><i class="fa fa-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li>
						

						<li>
							<a href="<?php echo base_url().'admin/pengguna'?>">
								<div class="gui-icon"><i class="fa fa-user"></i></div>
								<span class="title">Admin</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/menu'?>">
								<div class="gui-icon"><i class="fa fa-cutlery"></i></div>
								<span class="title">Menu</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/pelanggan'?>">
								<div class="gui-icon"><i class="fa fa-users"></i></div>
								<span class="title">Pelanggan</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/order'?>">
								<div class="gui-icon"><i class="fa fa-cart-arrow-down"></i></div>
								<span class="title">Pesanan</span>
							</a>
						</li>
						<!--<li>
							<a href="<?php echo base_url().'admin/nota'?>">
								<div class="gui-icon"><i class="fa fa-cart-arrow-down"></i></div>
								<span class="title">Nota</span>
							</a>
						</li>-->
						<li>
							<a href="<?php echo base_url().'admin/rekening'?>">
								<div class="gui-icon"><i class="fa fa-credit-card"></i></div>
								<span class="title">Rekening</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/konfirmasi'?>">
								<div class="gui-icon"><i class="fa fa-exchange"></i></div>
								<span class="title">Konfirmasi</span>
							</a>
						</li>

						
							<ul>
								<li><a href="<?php echo base_url().'admin/status'?>" ><span class="title">Status Pesanan</span></a></li>
							</ul>
						</li>


					</ul>

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							 <?php echo '2019';?>
						</small>
					</div>
				</div>
			</div>
			

			

		</div>
		

		
		<script src="<?php echo base_url().'assets/js/jquery/jquery-1.11.2.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery/jquery-migrate-1.2.1.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/bootstrap/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/spin/spin.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/autosize/jquery.autosize.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/moment/moment.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/jquery.flot.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/jquery.flot.time.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/jquery.flot.resize.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/jquery.flot.orderBars.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/jquery.flot.pie.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/flot/curvedLines.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery-knob/jquery.knob.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/sparkline/jquery.sparkline.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/nanoscroller/jquery.nanoscroller.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/d3/d3.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/d3/d3.v3.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/rickshaw/rickshaw.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/App.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavigation.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppOffcanvas.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppCard.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppForm.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavSearch.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppVendor.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/Chart.js'?>"></script>
		
		<script>

			var lineChartData = {
				labels : <?php echo json_encode($bulan);?>,
				datasets : [
					
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : <?php echo json_encode($value);?>
					}
				]
				
			}

		var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);

		var lineChartPelanggan = {
				labels : <?php echo json_encode($bln);?>,
				datasets : [
					
					{
						fillColor : "rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,1)",
						pointColor : "rgba(220,220,220,1)",
						pointStrokeColor : "#fff",
						data : <?php echo json_encode($val);?>
					}
				]
				
			}

		var myLineplg = new Chart(document.getElementById("canvasplg").getContext("2d")).Line(lineChartPelanggan);
		
		</script>
	</body>
</html>
