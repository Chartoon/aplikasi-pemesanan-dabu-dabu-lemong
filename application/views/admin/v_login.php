<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Masuk</title>

	

	
		<link href="<?php echo base_url().'assets/style-material.css'?>" rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/materialadmin.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/material-design-iconic-font.min.css'?>" />
		

		
	</head>
	<body class="menubar-hoverable header-fixed ">

		
		<section class="section-account">
			<div class="img-backdrop" style="background-image: url(<?php echo base_url().'assets/img/test.jpg'?>)"></div>
			<div class="spacer"></div>
			<div class="card contain-sm style-transparent">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-6">
							<br/>
							<span class="text-lg text-bold text-primary">MASUK ADMINISTRATOR</span>
							<br/><br/>
							<?php echo $this->session->flashdata('msg');?>
							<form class="form floating-label" action="<?php echo base_url().'administrator/auth'?>" accept-charset="utf-8" method="post">
								<div class="form-group">
									<input type="text" class="form-control" id="username" name="username" required>
									<label for="username">Username</label>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="password" name="password" required>
									<label for="password">Kata Sandi</label>
									
								</div>
								<br/>
								<div class="row">
									<div class="col-xs-6 text-left">
										<div class="checkbox checkbox-inline checkbox-styled">
											<label>
												<input type="checkbox"> <span>Tetap Masuk</span>
											</label>
										</div>
									</div>
									<div class="col-xs-6 text-right">
										<button class="btn btn-primary btn-raised" type="submit"><span class="fa fa-lock"></span> Masuk</button>
									</div>
								</div>
							</form>
						</div>
						
							</div>
						</div>
					</div>
				</section>
				

				
				<script src="<?php echo base_url().'assets/js/jquery/jquery-1.11.2.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/jquery/jquery-migrate-1.2.1.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/bootstrap/bootstrap.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/spin/spin.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/autosize/jquery.autosize.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/nanoscroller/jquery.nanoscroller.min.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/App.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppNavigation.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppOffcanvas.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppCard.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppForm.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppNavSearch.js'?>"></script>
				<script src="<?php echo base_url().'assets/js/source/AppVendor.js'?>"></script>
				

			</body>
		</html>
