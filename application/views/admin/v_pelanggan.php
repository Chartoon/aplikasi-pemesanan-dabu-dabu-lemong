<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Pelanggan</title>

		

		
		<link href="<?php echo base_url().'assets/style-material.css'?>" rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/materialadmin.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/material-design-iconic-font.min.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/jquery.dataTables.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/extensions/dataTables.colVis.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'assets/css/DataTables/extensions/dataTables.tableTools.css'?>" />


		
		<?php 
            function limit_words($string, $word_limit){
                $words = explode(" ",$string);
                return implode(" ",array_splice($words,0,$word_limit));
            }      
        ?>
	</head>
	<body class="menubar-hoverable header-fixed ">

		<?php 
			$this->load->view('admin/v_header');
		?>

	
		<div id="base">

		
			<div class="offcanvas">

			</div>

			
			<div id="content">
				<section>
					<div class="section-header">
							<h2><span class=""></span> Data Pelanggan</h2>
					</div>
						<?php echo $this->session->flashdata('msg');?>
				</section>

			
				<section class="style-default-bright" style="margin-top:0px;">
					
					
					<div class="section-body">	
						<div class="row">
							
							<table class="table table-hover" id="datatable1">
							<thead>
								<tr>
									<th>Foto</th>
									<th>Nama</th>
									<th>Jenis Kelamin</th>
									<th>Alamat</th>
									<th>Kontak</th>
									<th>Email</th>
									<th class="text-right">Aksi</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$no=0;
								foreach ($data->result_array() as $a) {
									$no++;
									$id=$a['plg_id'];
									$nama=$a['plg_nama'];	
									$alamat=$a['plg_alamat'];
									$jenkel=$a['plg_jenkel'];
									$notelp=$a['plg_notelp'];
									$email=$a['plg_email'];
									$photo=$a['plg_photo'];
									$register=$a['plg_register'];
								
							?>
								<tr>
									<?php if(empty($photo)):?>
										<td><img style="width:40px;height:40px;" class="img-circle width-1" src="<?php echo base_url().'assets/images/user_blank.png';?>" alt="" /></td>
									<?php else:?>
										<td><img style="width:40px;height:40px;" class="img-circle width-1" src="<?php echo base_url().'assets/images/'.$photo;?>" alt="" /></td>
									<?php endif;?>
									<td><?php echo $nama;?></td>
									<?php if($jenkel=='L'):?>
										<td>Laki-Laki</td>
									<?php else:?>
										<td>Perempuan</td>
									<?php endif;?>
									<td><?php echo $alamat;?></td>
									<td><?php echo $notelp;?></td>
									<td><?php echo $email;?></td>
									<td class="text-right">
										<a href="#" class="btn btn-icon-toggle" title="Lihat Detail" data-toggle="modal" data-target="#modal_edit_pengguna<?php echo $id;?>"><i class="fa fa-eye"></i></a>
										
										<a href="#" class="btn btn-icon-toggle" title="Hapus Pelanggan" data-toggle="modal" data-target="#modal_hapus_pengguna<?php echo $id;?>"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>

							<?php } ?>
								
							</tbody>
						  </table>

						</div>
					</div>

					
				</section>
			

				

			</div>

			
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					
				</div>
				<div class="menubar-scroll-panel">

					
					<ul id="main-menu" class="gui-controls">

						
						<li>
							<a href="<?php echo base_url().'admin/dashboard'?>" >
								<div class="gui-icon"><i class="fa fa-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/pengguna'?>">
								<div class="gui-icon"><i class="fa fa-user"></i></div>
								<span class="title">Admin</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/menu'?>">
								<div class="gui-icon"><i class="fa fa-cutlery"></i></div>
								<span class="title">Menu</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/pelanggan'?>" class="active">
								<div class="gui-icon"><i class="fa fa-users"></i></div>
								<span class="title">Pelanggan</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/order'?>">
								<div class="gui-icon"><i class="fa fa-cart-arrow-down"></i></div>
								<span class="title">Pesanan</span>
							</a>
						</li>
					
						<li>
							<a href="<?php echo base_url().'admin/rekening'?>">
								<div class="gui-icon"><i class="fa fa-credit-card"></i></div>
								<span class="title">Rekening</span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url().'admin/konfirmasi'?>">
								<div class="gui-icon"><i class="fa fa-exchange"></i></div>
								<span class="title">Konfirmasi</span>
							</a>
						</li>

						

						
							<ul>
								<li><a href="<?php echo base_url().'admin/status'?>" ><span class="title">Status Pesanan</span></a></li>
							</ul>
						</li>


					</ul>

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							 <?php echo '2019';?>
						</small>
					</div>
				</div>
			</div>

		</div>

			

			<!-- EDIT PENGGUNA  -->
			<?php 
				foreach ($data->result_array() as $a) {
					$id=$a['plg_id'];
					$nama=$a['plg_nama'];	
					$alamat=$a['plg_alamat'];
					$jenkel=$a['plg_jenkel'];
					$notelp=$a['plg_notelp'];
					$email=$a['plg_email'];
					$photo=$a['plg_photo'];
					$register=$a['plg_register'];
								
			?>
			<div class="modal fade" id="modal_edit_pengguna<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
			    <div class="modal-dialog">
			    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h3 class="modal-title" id="myModalLabel">Detail Pelanggan</h3>
			    </div>
			    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'admin/menu/update_menu'?>" enctype="multipart/form-data">
			        <div class="modal-body">
							<table>
								<tr>
									<td style="width:90px;">Nama</td>
									<td>:</td>
									<td style="width:160px;"><?php echo $nama;?></td>
									<td style="width:90px;">Jenis Kelamin</td>
									<td>:</td>
									<?php if($jenkel=='L'):?>
										<td>Laki-Laki</td>
									<?php else:?>
										<td>Perempuan</td>
									<?php endif;?>
								</tr>
								<tr>
									<td style="width:90px;">Alamat</td>
									<td>:</td>
									<td style="width:160px;"><?php echo $alamat;?></td>
									<td style="width:90px;">Kontak</td>
									<td>:</td>
									<td><?php echo $notelp;?></td>
								</tr>
								<tr>
								
									<td style="width:90px;">Email</td>
									<td>:</td>
									<td><?php echo $email;?></td>
								</tr>
								
								
							</table>			
									
			        </div>
			        <div class="modal-footer">
			            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
			        </div>
			    </form>
			    </div>
			    </div>
			</div>
			<?php } ?>

			<!--  HAPUS PENGGUNA  -->
			<?php 
				foreach ($data->result_array() as $a) {
					$id=$a['plg_id'];
					$nama=$a['plg_nama'];	
					$alamat=$a['plg_alamat'];
					$jenkel=$a['plg_jenkel'];
					$notelp=$a['plg_notelp'];
					$email=$a['plg_email'];
					
					$photo=$a['plg_photo'];
					$register=$a['plg_register'];
								
			?>
			<div class="modal fade" id="modal_hapus_pengguna<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
			    <div class="modal-dialog">
			    <div class="modal-content">
			    <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h3 class="modal-title" id="myModalLabel">Hapus Pelanggan</h3>
			    </div>
			    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'admin/pelanggan/hapus_pelanggan'?>" enctype="multipart/form-data">
			        <div class="modal-body">
									<div class="form-group">
										<label for="regular13" class="col-sm-2 control-label"></label>
										<div class="col-sm-8">
											<input type="hidden" name="kode" value="<?php echo $id;?>">
											<input type="hidden" name="nama" value="<?php echo $nama;?>">
											<p>Apakah Anda yakin mau menghapus data <b><?php echo $nama;?></b> ?</p>
										</div>
									</div>
	
			        </div>
			        <div class="modal-footer">
			            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
			            <button class="btn btn-primary" type="submit"><span class="fa fa-trash"></span> Hapus</button>
			        </div>
			    </form>
			    </div>
			    </div>
			</div>
			<?php } ?>

		
		<script src="<?php echo base_url().'assets/js/jquery/jquery-1.11.2.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/jquery/jquery-migrate-1.2.1.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/bootstrap/bootstrap.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/spin/spin.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/autosize/jquery.autosize.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/jquery.dataTables.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/extensions/ColVis/js/dataTables.colVis.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/nanoscroller/jquery.nanoscroller.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/App.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavigation.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppOffcanvas.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppCard.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppForm.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppNavSearch.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/source/AppVendor.js'?>"></script>
		<script src="<?php echo base_url().'assets/js/core/DemoTableDynamic.js'?>"></script>
		

	</body>
</html>
