<!DOCTYPE html>
<html>
	<head>
		

		<title>Menu</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/jquery.mmenu.all.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/style.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/jquery.mobile-1.0rc2.min.css'?>" />

		<link rel="apple-touch-icon" href="<?php echo base_url().'mobile/img/apple-touch-icon.png'?>">
		<link rel="apple-touch-startup-image" href="<?php echo base_url().'mobile/img/apple-touch-startup-image.png'?>">
		
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.mmenu.min.all.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.easy-pie-chart.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/o-script.js'?>"></script>
	</head>
	<body class="o-page p-blog">
		<div id="page">
			<div id="header">
				<div class="header-content">
					<a href="#menu" class="p-link-home"><i class="fa fa-bars"></i></a>
					<a href="javascript:history.back();" class="p-link-back"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			<div class="bannerPane banner-bg">
				<div class="overlay"></div>
				<div class="s-banner-content">
					<?php echo $judul;?>
				</div>
			</div>	
			<div id="content">

			<p style="font-size: 230%; color:red; text-align:center; border: solid; margin:  0px 0px 0px 0px;" ><?=$this->session->userdata('nama_pel');?></p>
			
				<?php 
				foreach ($data->result_array() as $a) {
					$id=$a['menu_id'];
					$nama=$a['menu_nama'];	
					$deskripsi=$a['menu_deskripsi'];
					$harga_lama=$a['menu_harga_lama'];
					$harga_baru=$a['menu_harga_baru'];
					$harlam=$a['harga_lama'];
					$harbar=$a['harga_baru'];
					$kat_id=$a['menu_kategori_id'];
					$kat_nama=$a['menu_kategori_nama'];
					$gambar=$a['menu_gambar'];

				?>
				
				<article>
					<a href="<?php echo base_url().'mobile/menu/detail_menu/'.$id;?>">
						<div class="article-img-pane">
							<img src="<?php echo base_url().'assets/gambar/'.$gambar;?>" />
						</div>
						<h2><?php echo $nama;?></h2>
						<div class="prod-pricePane">
							<?php if(empty($harga_lama)):?>
								<span class="current-price pull-right"><?php echo $harbar.'K';?></span>
							<?php else:?>
								<span class="current-price pull-right"><?php echo $harbar.'K';?></span>
								<span class="last-price pull-right"><?php echo $harlam.'K';?></span>
							<?php endif;?>
						</div>
					</a>
					<div class="a-meta">
						
						
						
					</div>
				</article>
				 <?php }?>

			</div>
			
			
			
			<nav id="menu">
				<ul>
					<li class="Selected">
						<a href="#close">
							<i class="fa fa-times-circle"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/home'?>">
							<i class="fa fa-home"></i>Beranda
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/cart'?>">
							<i class="fa fa-shopping-cart"></i>Daftar Pesanan(<?=$this->cart->total_items();?>)
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/makanan'?>">
							<i class="fa fa-cutlery"></i>Makanan
						</a>
					</li>

					<li>
						<a href="<?php echo base_url().'mobile/menu/minuman'?>">
							<i class="fa fa-glass"></i>Minuman
						</a>
					</li>
					
				
					
					<li>
						<a href="<?php echo base_url().'mobile/tracker'?>">
							<i class="fa fa-crosshairs"></i>Lacak Pesanan
						</a>
					</li>
					
					
					<?php if($this->session->userdata('online') == TRUE):?>
					<li>
						<a href="<?php echo base_url().'mobile/konfirmasi'?>">
							<i class="fa fa-exchange"></i>Konfirmasi
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url().'mobile/member/logout'?>">
							<i class="fa fa-sign-out"></i>Keluar
						</a>
					</li>
					<?php else:?>
					<li>
						<a href="<?php echo base_url().'mobile/member'?>">
							<i class="fa fa-sign-in"></i>Masuk
						</a>
					</li>
					<?php endif;?>
				</ul>
					
			</nav>
			
		</div>
	</body>
</html>