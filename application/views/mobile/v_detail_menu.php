<!DOCTYPE html>
<html>
	<head>
		

		<title>Detail Menu</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/jquery.mmenu.all.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/style.css'?>" />

		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
		<link rel="apple-touch-startup-image" href="img/apple-touch-startup-image.png">
		
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.mmenu.min.all.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.easy-pie-chart.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/o-script.js'?>"></script>
		<?php 
			$b=$data->row_array();
			$url=base_url().'mobile/menu/detail_menu/'.$b['menu_id'];
	        $img=base_url().'assets/gambar/'.$b['menu_gambar'];
	   		$title=$b['menu_nama'];
			$deskripsi=strip_tags($b['menu_deskripsi']);
		?>
		
	</head>
	
	<body class="o-page">
		<div id="page">
			<div id="header">
				<div class="header-content">
					<a href="#menu" class="p-link-home"><i class="fa fa-bars"></i></a>
					<a href="javascript:history.back();" class="p-link-back"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			<div class="bannerPane banner-bg">
				<div class="overlay"></div>
				<div class="s-banner-content">
					DETAIL MENU
				</div>
				</div>	
				<div id="content" style="border: solid; margin:  20px 500px 500px 470px;">
				<img src="<?php echo base_url().'assets/gambar/'.$b['menu_gambar']?>" style="width:200px"/>
				<div class="pull-right">
				
							<p style="font-size: 250%"><?php echo $b['menu_nama']?></p>
							Deskripsi :
							<?php echo $b['menu_deskripsi']?><br>
							Harga :
							<?php if(empty($b['menu_harga_lama'])):?>
							<span class="current-price" style="font-size:20px;"><?php echo 'Rp '.number_format($b['menu_harga_baru'])?></span>
						<?php else:?>
							<span class="current-price" style="font-size:20px;"><?php echo 'Rp '.number_format($b['menu_harga_baru'])?></span>
							<span class="last-price" style="font-size:12px;"><?php echo $b['harga_lama'].'K'?></span>
						<?php endif;?><br><br><br>
							<a href="<?php echo base_url().'mobile/menu/add_to_cart/'.$b['menu_id'];?>" style="text-decoration:none;"><div class="subFooter"><span class="fa fa-shopping-cart"></span> Beli</div></a>
				
					</div>
			</div>
		
			
			<!-- Menu navigation -->
			<nav id="menu">
				<ul>
					<li class="Selected">
						<a href="#close">
							<i class="fa fa-times-circle"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/home'?>">
							<i class="fa fa-home"></i>Beranda
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/cart'?>">
							<i class="fa fa-shopping-cart"></i>Daftar Pesanan (<?=$this->cart->total_items();?>)
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/makanan'?>">
							<i class="fa fa-cutlery"></i>Makanan
						</a>
					</li>

					<li>
						<a href="<?php echo base_url().'mobile/menu/minuman'?>">
							<i class="fa fa-glass"></i>Minuman
						</a>
					</li>
					
					
					
					<li>
						<a href="<?php echo base_url().'mobile/tracker'?>">
							<i class="fa fa-crosshairs"></i>Lacak Pesanan
						</a>
					</li>
					
					<?php if($this->session->userdata('online') == TRUE):?>
					<li>
						<a href="<?php echo base_url().'mobile/konfirmasi'?>">
							<i class="fa fa-exchange"></i>Konfirmasi
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url().'mobile/member/logout'?>">
							<i class="fa fa-sign-out"></i>Keluar
						</a>
					</li>
					<?php else:?>
					<li>
						<a href="<?php echo base_url().'mobile/member'?>">
							<i class="fa fa-sign-in"></i>Masuk
						</a>
					</li>
					<?php endif;?>
				</ul>
					
			</nav>
			
		</div>
	</body>
</html>