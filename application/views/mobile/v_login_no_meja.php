<!DOCTYPE html>
<html>
	<head>
		
		<title>Login</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/jquery.mmenu.all.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/style.css'?>" />

		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
		<link rel="apple-touch-startup-image" href="img/apple-touch-startup-image.png">
		
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.mmenu.min.all.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/gmap3.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/o-script.js'?>"></script>

	</head>
	<body class="o-page p-home">
		<div id="page">
			<div id="header">
				<div class="header-content">
					
					<a href="javascript:history.back();" class="p-link-back"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			<div class="bannerPane banner-bg">
				<div class="overlay"></div>
				<div class="s-banner-content">
					<i class="fa fa-user"></i> Masuk
				</div>
			</div>
	
			
			<div id="content">
				
				<?php echo $this->session->flashdata('msg');?>
				
				<form class="contactForm" action="<?php echo base_url().'mobile/member/auth_meja/'?>" method="post">
					
					<input type="text" name="meja" placeholder=" Nomor Meja" required>
					<button type="submit" class="o-buttons red" style="border:none;height:38px;width:70px;">Masuk</button>
				</form>
					
				
			</div>
			
		</div>
	</body>
</html>