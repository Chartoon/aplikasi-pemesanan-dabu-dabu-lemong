<!DOCTYPE html>
<html >
	<head>
		

		<title>Beranda</title>
		<link href="<?php echo base_url().'assets/img/logo.png'?>" rel="shortcut icon" type="image/x-icon">

		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/jquery.mmenu.all.css'?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url().'mobile/css/style.css'?>" />
		<link rel='stylesheet' id='camera-css'  href="<?php echo base_url().'mobile/css/camera.css'?>" type='text/css' media='all'> 
		
		
		
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/jquery.mmenu.min.all.js'?>"></script>
		<script type="text/javascript" src="<?php echo base_url().'mobile/js/o-script.js'?>"></script>
		
		<script type='text/javascript' src="<?php echo base_url().'mobile/js/slider/jquery.mobile.customized.min.js'?>"></script>
		<script type='text/javascript' src="<?php echo base_url().'mobile/js/slider/jquery.easing.1.3.js'?>"></script> 
		<script type='text/javascript' src="<?php echo base_url().'mobile/js/slider/camera.min.js'?>"></script> 
		
		<script>
			jQuery(function(){
				
				jQuery('#camera_wrap_4').camera({
					height: 'auto',
					loader: '',
					pagination: false,
					thumbnails: false,
					hover: false,
					opacityOnGrid: false
				});

			});
		</script>
	</head>
	<body class="o-page p-home">
		<div id="page">
			
			<div id="header">
				<div class="header-content">
				
				<span id="Logo" class="svg">
						<img src="<?php echo base_url().'assets/img/logo.png'?>" />
					</span>
					<a href="" class="p-link-home"><i class=""></i></a>
					<?php if($this->session->userdata('online')==false):?>
						<a href="" class=""><i class=""></i></a>
					<?php else:?>
						 <a href="<?php echo base_url().'mobile/member/logout'?>" class="p-link-back"><i class="fa fa-sign-out"></i></a> 
					<?php endif;?>
				</div>
			</div>
			
			
			<div id="content">
		
			<span id="Logo" class="svg">
						<img src="<?php echo base_url().'assets/img/logo.png'?>" />
					</span>
				<div class="home-content">
				
				</div>
				<div class="fluid_container">
					<div class="camera_wrap camera_black_skin camera_emboss pattern_1" id="camera_wrap_4">
						<?php 
							foreach ($data->result_array() as $a) {
								$id=$a['menu_id'];
								$nama=$a['menu_nama'];	
								$deskripsi=$a['menu_deskripsi'];
								//$harga_lama=$a['menu_harga_lama'];
								$harga_baru=$a['menu_harga_baru'];
								
								$kat_id=$a['menu_kategori_id'];
								$kat_nama=$a['menu_kategori_nama'];
								$gambar=$a['menu_gambar'];

							?>
						<div data-thumb="<?php echo base_url().'assets/gambar/'.$gambar;?>" data-src="<?php echo base_url().'assets/gambar/'.$gambar;?>">
							<div class="bannerContent fadeFromBottom">
								<div class="b-c-textpane">
									<h1><?php echo number_format($harga_baru);?></h1>
								
									<a href="<?php echo base_url().'mobile/member/'?>" class="home-scl-icon gplus">
										<i class="fa fa-arrow-right"></i>
										<div>Saya Berada Di luar Rumah Makan</div>
									</a>

									<a href="<?php echo base_url().'mobile/member/no_meja'?>" class="home-scl-icon gplus">
										<i class="fa fa-arrow-right"></i>
										
										<div>Saya Berada Di tempat</div>
									</a>
								</div>
							</div>
						</div>

						<?php }?>
						
					</div> 

				</div>
				
				
			</div>
			
			<!-- Menu navigation -->
			<nav id="menu">
				<ul>
					<li class="Selected">
						<a href="#close">
							<i class="fa fa-times-circle"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/cart'?>">
							<i class="fa fa-shopping-cart"></i>Daftar Pesanan (<?=$this->cart->total_items();?>)
						</a>
					</li>
					<li>
						<a href="<?php echo base_url().'mobile/menu/makanan'?>">
							<i class="fa fa-cutlery"></i>Makanan
						</a>
					</li>

					<li>
						<a href="<?php echo base_url().'mobile/menu/minuman'?>">
							<i class="fa fa-glass"></i>Minuman
						</a>
					</li>
				
					
					
					<li>
						<a href="<?php echo base_url().'mobile/tracker'?>">
							<i class="fa fa-crosshairs"></i>Lacak Pesanan
						</a>
					</li>
					
					
					<?php if($this->session->userdata('online') == TRUE):?>
					<li>
						<a href="<?php echo base_url().'mobile/konfirmasi'?>">
							<i class="fa fa-exchange"></i>Konfirmasi
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url().'mobile/member/logout'?>">
							<i class="fa fa-sign-out"></i>Keluar
						</a>
					</li>
					<?php else:?>
					<li>
						<a href="<?php echo base_url().'mobile/member'?>">
							<i class="fa fa-sign-in"></i>Masuk
						</a>
					</li>
					<?php endif;?>
				</ul>
			</nav>
			
		</div>
	</body>
</html>