<?php
class Nota extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_order');
		$this->load->model('m_status');
		$this->load->model('m_pengguna');
	}

	function index(){
		$kode=$this->session->userdata('idadmin');
		$x['user']=$this->m_pengguna->get_pengguna_login($kode);
		$x['data']=$this->m_order->get_all_order();
		$x['stts']=$this->m_status->get_all_status();
		$this->load->view('admin/v_nota',$x);
	}

	


}