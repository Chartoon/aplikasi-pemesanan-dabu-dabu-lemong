<?php
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_menu');
		$this->load->model('m_kategori');
		$this->load->library('upload');
	}


	function index(){
		
		$x['data']=$this->m_menu->makanan();
		$this->load->view('mobile/v_home',$x);
	}

}