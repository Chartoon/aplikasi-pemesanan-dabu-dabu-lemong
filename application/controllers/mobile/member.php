<?php
class Member extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pelanggan');
		$this->load->library('upload');
	}


	function index(){
		
		$this->load->view('mobile/v_login');
    }
    
    function no_meja(){
		
		$this->load->view('mobile/v_login_no_meja');
	}

    function register(){
        $this->load->view('mobile/v_register');
    }
    function simpan_pelanggan(){

        $nmfile = "file_".time(); 
                $config['upload_path'] = './assets/images/'; 
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['max_size'] = '1024'; 
                $config['max_width']  = '900'; 
                $config['max_height']  = '800'; 
                $config['file_name'] = $nmfile; 
                
                $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $code = substr(str_shuffle($set), 0, 15);

                
			$config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'charlietjiptomo35@gmail.com',
                'smtp_pass' => 'quiksilversk9',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
          );    

          $email = strip_tags(str_replace("'", "", $this->input->post('email')));
          
			$message = 	"
            <html>
            <head>
                <title>Verification Code</title>
            </head>
            <body>
                <h2>Terima kasih telah mendaftar di Aplikasi Pemesanan Makanan dan Minuman Dabu Dabu Lemong</h2>
                <p>Lakukan verifikasi untuk melanjutkan pendaftaran</p>
                <p>Akun anda: ".strip_tags(str_replace("'", "", $this->input->post('nama')))."</p>
                <p>Klik link dibawah untuk mengaktifkan akun anda</p>
                <h4><a href='".base_url()."user/activate/".strip_tags(str_replace("'", "", $this->input->post('email')))."/".$code."'>Aktifkan Akun Saya</a></h4>
            </body>
            </html>
            ";

            $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");
		    $this->email->from($config['smtp_user']);
		    $this->email->to($email);
		    $this->email->subject('Signup Verification Email');
		    $this->email->message($message);

                $this->upload->initialize($config);
                if(!empty($_FILES['filefoto']['name']))
                {
                    if ($this->upload->do_upload('filefoto'))
                    {
                            $gbr = $this->upload->data();
                            $gambar=$gbr['file_name'];
                            $nama=strip_tags(str_replace("'", "", $this->input->post('nama')));
                            $alamat=strip_tags(str_replace("'", "", $this->input->post('alamat')));
                            $jenkel=strip_tags(str_replace("'", "", $this->input->post('jenkel')));
                            $kontak=strip_tags(str_replace("'", "", $this->input->post('kontak')));
                            $email=strip_tags(str_replace("'", "", $this->input->post('email')));
                            $pass=strip_tags(str_replace("'", "", $this->input->post('pass')));
                            $pass2=strip_tags(str_replace("'", "", $this->input->post('pass2')));
                            if ($pass <> $pass2) {
                                echo $this->session->set_flashdata('msg','<div class="notifications warning">Password dan Ulangi Password yang Anda masukan tidak sama.</div>');
                                redirect('mobile/member/register');
                            }else{
                                
                                $this->m_pelanggan->simpan_pelanggan_dengan_gambar($nama,$alamat,$jenkel,$kontak,$email,$pass,$gambar);
                                if($this->email->send()):
                                    echo $this->session->set_flashdata('msg','<div class="notifications success">Data <b>'.$nama.'</b> Berhasil di simpan ke database.</div>');
                                    redirect('mobile/member');
                                else:
                                    echo $this->session->set_flashdata('msg','<div class="notifications success">Data <b>'.$nama.'</b> Berhasil di simpan ke database, email gagal dikirim. '.$this->email->print_debugger().'</div>');
                                    redirect('mobile/member');
                                endif;
                            }
                        
                    }else{
                        echo $this->session->set_flashdata('msg','<div class="notifications error">Data tidak dapat di simpan, file gambar yang Anda masukkan terlalu besar.</div>');
                        redirect('mobile/member/register');
                    }
        
                }else{
                    $nama=strip_tags(str_replace("'", "", $this->input->post('nama')));
                    $alamat=strip_tags(str_replace("'", "", $this->input->post('alamat')));
                    $jenkel=strip_tags(str_replace("'", "", $this->input->post('jenkel')));
                    $kontak=strip_tags(str_replace("'", "", $this->input->post('kontak')));
                    $email=strip_tags(str_replace("'", "", $this->input->post('email')));
                    $pass=strip_tags(str_replace("'", "", $this->input->post('pass')));
                    $pass2=strip_tags(str_replace("'", "", $this->input->post('pass2')));
                    if ($pass <> $pass2){
                        echo $this->session->set_flashdata('msg','<div class="notifications warning">Password dan Ulangi Password yang Anda masukan tidak sama.</div>');
                        redirect('mobile/member/register');
                    }else{
                        $this->m_pelanggan->simpan_pelanggan_tanpa_gambar($nama,$alamat,$jenkel,$kontak,$email,$pass);
                        if($this->email->send()):
                            echo $this->session->set_flashdata('msg','<div class="notifications success">Data <b>'.$nama.'</b> Berhasil di simpan ke database.</div>');
                            redirect('mobile/member');
                        else:
                            echo $this->session->set_flashdata('msg','<div class="notifications success">Data <b>'.$nama.'</b> Berhasil di simpan ke database, email gagal dikirim. '.$this->email->print_debugger().'</div>');
                            redirect('mobile/member');
                        endif;
                    }
                } 

        
    }

	function auth(){
        $username=strip_tags(str_replace("'", "", $this->input->post('email')));
        $password=strip_tags(str_replace("'", "", $this->input->post('pass')));
        $u=$username;
        $p=$password;
        $cadmin=$this->m_pelanggan->cek_pelanggan($u,$p);
        if($cadmin->num_rows > 0){
         $this->session->set_userdata('online',true);
         $this->session->set_userdata('pengguna',$u);
         $this->session->set_userdata('hakakses',3);
         $xcadmin=$cadmin->row_array();
         $this->session->set_userdata('nama_pel',$xcadmin['plg_nama']); 
         $this->session->set_userdata('kopel',$xcadmin['plg_id']); 
         $this->session->set_userdata('mode','noncod'); 
        }else{
                $this->session->set_userdata('online',false);
        }
        if($this->session->userdata('online')==true){
            redirect('mobile/member/berhasillogin');
        }else{
            redirect('mobile/member/gagallogin');
        }
    }

    function auth_meja(){
        $username=strip_tags(str_replace("'", "", $this->input->post('meja')));
        $u=$username;
        $cadmin=$this->m_pelanggan->cek_meja($u);
        if($cadmin->num_rows > 0){
         $this->session->set_userdata('online',true);
         $this->session->set_userdata('pengguna',$u);
         $this->session->set_userdata('hakakses',3);
         $xcadmin=$cadmin->row_array();
         $this->session->set_userdata('nama_pel',$xcadmin['plg_nama']); 
         $this->session->set_userdata('kopel',$xcadmin['plg_id']); 
         $this->session->set_userdata('mode','cod'); 
        }else{
                $this->session->set_userdata('online',false);
        }
        if($this->session->userdata('online')==true){
            redirect('mobile/member/berhasillogin');
        }else{
            redirect('mobile/member/no_meja/gagallogin');
        }
    }


    function berhasillogin(){
            if(empty($this->cart->total_items())){
                $kopel=$this->session->userdata('kopel');
                $this->db->query("update tbl_pelanggan set plg_status='1' where plg_id='$kopel'");
                redirect('mobile/menu');
            }else{
                redirect('mobile/menu/cart');
            }
            
    }

    function gagallogin(){
            $url=base_url('mobile/member');
            echo $this->session->set_flashdata('msg','<div class="notifications error"><i class="fa fa-exclamation-circle"></i> Email atau Password yang anda masukan salah. Mohon Check Kembali!</div>');
            redirect($url);
    }
    
    function logout(){
            $kopel=$this->session->userdata('kopel');
            $this->db->query("update tbl_pelanggan set plg_status='0' where plg_id='$kopel'");
            $this->session->sess_destroy();
            $url=base_url('mobile/home');
            redirect($url);
    }


}