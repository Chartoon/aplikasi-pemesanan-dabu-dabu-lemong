-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2019 at 09:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dabudabulemong`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail`
--

CREATE TABLE `tbl_detail` (
  `detail_id` int(11) NOT NULL,
  `detail_menu_id` int(11) DEFAULT NULL,
  `detail_menu_nama` varchar(100) DEFAULT NULL,
  `detail_harjul` double DEFAULT NULL,
  `detail_porsi` int(11) DEFAULT NULL,
  `detail_subtotal` double DEFAULT NULL,
  `detail_inv_no` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail`
--

INSERT INTO `tbl_detail` (`detail_id`, `detail_menu_id`, `detail_menu_nama`, `detail_harjul`, `detail_porsi`, `detail_subtotal`, `detail_inv_no`) VALUES
(156, 27, 'Ikan Gurame Bakar', 30000, 1, 30000, 'INV271119000001'),
(157, 27, 'Ikan Gurame Bakar', 30000, 1, 30000, 'INV271119000002'),
(158, 27, 'Ikan Gurame Bakar', 30000, 1, 30000, 'INV271119000003'),
(159, 27, 'Ikan Gurame Bakar', 30000, 1, 30000, 'INV271119000004'),
(160, 27, 'Ikan Gurame Bakar', 30000, 1, 30000, 'INV271119000005');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice`
--

CREATE TABLE `tbl_invoice` (
  `inv_no` varchar(15) NOT NULL,
  `inv_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `inv_plg_id` int(11) DEFAULT NULL,
  `inv_plg_nama` varchar(80) DEFAULT NULL,
  `inv_status` varchar(40) DEFAULT NULL,
  `inv_total` double DEFAULT NULL,
  `inv_rek_id` varchar(10) DEFAULT NULL,
  `inv_rek_no` varchar(60) DEFAULT NULL,
  `inv_rek_bank` varchar(30) DEFAULT NULL,
  `inv_rek_nama` varchar(50) DEFAULT NULL,
  `inv_rek_cabang` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invoice`
--

INSERT INTO `tbl_invoice` (`inv_no`, `inv_tanggal`, `inv_plg_id`, `inv_plg_nama`, `inv_status`, `inv_total`, `inv_rek_id`, `inv_rek_no`, `inv_rek_bank`, `inv_rek_nama`, `inv_rek_cabang`) VALUES
('INV271119000001', '2019-11-27 05:59:47', 28, 'Meja001', 'Pembayaran Selesai', 30000, 'Di Tempat', NULL, NULL, NULL, NULL),
('INV271119000002', '2019-11-27 06:00:20', 25, 'charlie', 'Pembayaran Selesai', 30000, '001', '123456789', 'BCA', 'Charlie Tjiptomo', 'Manado'),
('INV271119000003', '2019-11-27 06:08:58', 28, 'Meja001', 'Pembayaran Selesai', 30000, 'Di Tempat', NULL, NULL, NULL, NULL),
('INV271119000004', '2019-11-27 06:16:01', 28, 'Meja001', 'Pembayaran Selesai', 30000, 'Di Tempat', NULL, NULL, NULL, NULL),
('INV271119000005', '2019-11-27 06:17:07', 28, 'Meja001', 'Menunggu Konfirmasi', 30000, 'Di Tempat', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'Makanan'),
(2, 'Minuman');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konfirmasi`
--

CREATE TABLE `tbl_konfirmasi` (
  `konfirmasi_id` int(11) NOT NULL,
  `konfirmasi_invoice` varchar(15) DEFAULT NULL,
  `konfirmasi_nama` varchar(60) DEFAULT NULL,
  `konfirmasi_bank` varchar(50) DEFAULT NULL,
  `konfirmasi_jumlah` double DEFAULT NULL,
  `konfirmasi_bukti` varchar(20) DEFAULT NULL,
  `konfirmasi_status` int(11) DEFAULT '0',
  `konfirmasi_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_konfirmasi`
--

INSERT INTO `tbl_konfirmasi` (`konfirmasi_id`, `konfirmasi_invoice`, `konfirmasi_nama`, `konfirmasi_bank`, `konfirmasi_jumlah`, `konfirmasi_bukti`, `konfirmasi_status`, `konfirmasi_tanggal`) VALUES
(1, 'INV231216000001', 'M Fikri Setiadi', 'Bank BRI', 100000, 'file_1494171423.jpg', 1, '2017-05-07 15:37:03'),
(2, 'INV081119000001', 'charlie', 'BCA', 60000, 'file_1573186452.jpg', 1, '2019-11-08 04:14:12'),
(3, 'INV081119000002', 'Charlie Tjiptomo', 'BCA', 100000, 'file_1573196165.jpg', 1, '2019-11-08 06:56:05'),
(4, 'INV081119000003', 'Meja002', 'BCA', 1000000, 'file_1573197430.jpg', 1, '2019-11-08 07:17:10'),
(5, 'INV091119000001', 'Meja002', 'BCA', 100000, 'file_1573274904.jpg', 1, '2019-11-09 04:48:24'),
(6, 'INV091119000001', 'Meja001', 'BCA', 100000, 'file_1573319686.jpg', 1, '2019-11-09 17:14:46'),
(7, 'INV101119000002', 'Charlie', 'BCA', 100000, 'file_1573366878.jpg', 1, '2019-11-10 06:21:18'),
(8, 'INV111119000003', 'charlie', 'BCA', 10000, 'file_1573452754.png', 1, '2019-11-11 06:12:34'),
(9, 'INV121119000005', 'Charlie', 'BCA', 10000, 'file_1573570121.png', 1, '2019-11-12 14:48:41'),
(10, 'INV131119000002', 'Charlie', 'BCA', 100000, 'file_1573615511.jpg', 1, '2019-11-13 03:25:11'),
(11, 'INV141119000003', 'Charlie Tjiptomo', 'BCA', 10000, 'file_1573717303.jpg', 1, '2019-11-14 07:41:43'),
(12, 'INV171119000002', 'Meja002', 'BCA', 45000, 'file_1573951825.jpg', 1, '2019-11-17 00:50:25'),
(13, 'INV171119000004', 'Charlie Tjiptomo', 'BCA', 10000, 'file_1573951966.jpg', 1, '2019-11-17 00:52:46'),
(14, 'INV201119000003', 'ccdc', 'BCA', 100000, 'file_1574244166.jpg', 1, '2019-11-20 10:02:46'),
(15, 'INV221119000001', 'charlie', 'bca', 10000, 'file_1574390240.jpg', 1, '2019-11-22 02:37:20'),
(16, 'INV221119000008', 'Charlie Tjiptomo', 'BCA', 10000, 'file_1574410038.jpg', 1, '2019-11-22 08:07:18'),
(17, 'INV241119000002', 'dev', 'BCA', 10000, 'file_1574562073.jpg', 1, '2019-11-24 02:21:13'),
(18, 'INV241119000003', 'Charlie Tjiptomo', '', 100000, 'file_1574562885.jpg', 1, '2019-11-24 02:34:45'),
(19, 'INV241119000004', 'Charlie Tjiptomo', '', 10000, 'file_1574564962.jpg', 1, '2019-11-24 03:09:22'),
(20, 'INV241119000008', 'charlie', '', 20000, 'file_1574592027.jpg', 1, '2019-11-24 10:40:27'),
(21, 'INV251119000001', 'Charlie Tjiptomo', '', 60500, 'file_1574665930.jpg', 1, '2019-11-25 07:12:10'),
(22, 'INV251119000002', 'Charlie Tjiptomo', '', 100000, 'file_1574666621.jpg', 1, '2019-11-25 07:23:41'),
(23, 'INV271119000002', 'Charlie Tjiptomo', '', 30000, 'file_1574834446.jpg', 1, '2019-11-27 06:00:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_nama` varchar(100) DEFAULT NULL,
  `menu_deskripsi` varchar(200) DEFAULT NULL,
  `menu_harga_lama` double DEFAULT NULL,
  `menu_harga_baru` double DEFAULT NULL,
  `menu_kategori_id` int(11) DEFAULT NULL,
  `menu_kategori_nama` varchar(30) DEFAULT NULL,
  `menu_status` int(11) DEFAULT '1',
  `menu_gambar` varchar(30) DEFAULT NULL,
  `status_ketersediaan` set('Tersedia','Kosong') NOT NULL DEFAULT 'Tersedia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `menu_nama`, `menu_deskripsi`, `menu_harga_lama`, `menu_harga_baru`, `menu_kategori_id`, `menu_kategori_nama`, `menu_status`, `menu_gambar`, `status_ketersediaan`) VALUES
(2, 'Paket 1', 'Tuna Bakar + Sayur + Nasi', 20000, 25000, 1, 'Makanan', 1, 'file_1481423289.jpg', 'Kosong'),
(3, 'Paket 2', 'Rahang Tuna Bakar + Sayur + Nasi', 20000, 18000, 1, 'Makanan', 1, 'file_1481423323.jpg', 'Kosong'),
(4, 'Bakso Tusuk', 'Bakso Tusuk', NULL, 15000, 1, 'Makanan', 1, 'file_1481423391.jpg', 'Kosong'),
(5, 'Paket 3', 'Cumi Bakar + Sayur + Nasi', 20000, 17000, 1, 'Makanan', 1, 'file_1481423407.jpg', 'Kosong'),
(6, 'Nahe 1', 'Nasi Hemat (Nasi + Telur)', NULL, 20000, 1, 'Makanan', 1, 'file_1481423428.jpg', 'Kosong'),
(7, 'Nahe 2', 'Nasi Hemat (Nasi Goreng)', NULL, 15000, 1, 'Makanan', 1, 'file_1481505660.jpg', 'Kosong'),
(9, 'Pisang Goroho', 'Pisang Goroho', NULL, 15000, 1, 'Makanan', 1, 'file_1481505718.jpg', 'Kosong'),
(10, 'Indomie Goreng Beef', 'Indomie Goreng Beef', NULL, 20000, 1, 'Makanan', 1, 'file_1481505737.jpg', 'Tersedia'),
(11, 'Kopi Susu', 'Kopi Susu', NULL, 12000, 2, 'Minuman', 1, 'file_1494160941.jpg', 'Kosong'),
(12, 'Spritee', 'Spritee', 16000, 15000, 2, 'Minuman', 1, 'file_1494160974.jpg', 'Tersedia'),
(13, 'Ice Lemon', 'Ice Lemon', NULL, 12000, 2, 'Minuman', 1, 'file_1494161014.jpg', 'Tersedia'),
(14, 'Fanta', 'Fanta', NULL, 12000, 2, 'Minuman', 1, 'file_1494161073.jpg', 'Tersedia'),
(15, 'Coca Cola ', 'Coca Cola ', NULL, 12000, 2, 'Minuman', 1, 'file_1494161100.jpg', 'Kosong'),
(16, 'Es Teh', 'Es Teh', NULL, 10000, 2, 'Minuman', 1, 'file_1494161133.jpg', 'Tersedia'),
(17, 'Juice Lemon', 'Juice Lemon', NULL, 15000, 2, 'Minuman', 1, 'file_1494161156.jpg', 'Tersedia'),
(18, 'Roti 40cm', 'Roti 40cm', NULL, 16000, 1, 'Makanan', 1, 'file_1494161185.jpg', 'Kosong'),
(19, 'Ayam Lalapan', 'Ayam Lalapan + Nasi + Sayur', NULL, 20000, 1, 'Makanan', 1, 'file_1494161206.jpg', 'Tersedia'),
(20, 'Paket Mujair', 'Mujair Bakar + Nasi + Sayur', NULL, 25000, 1, 'Makanan', 1, 'file_1574830451.jpg', 'Kosong'),
(21, 'Tuna Bakar', 'Tuna Bakar + Nasi + Sayur', NULL, 25000, 1, 'Makanan', 1, 'file_1574830540.jpg', 'Kosong'),
(22, 'Paket 1', '2 Ikan Bakar + Nasi + Sayur', NULL, 30000, 1, 'Makanan', 1, 'file_1574830746.jpg', 'Kosong'),
(23, 'Paket 2', '2 Mujair Bakar + Nasi + Sayur', NULL, 35000, 1, 'Makanan', 1, 'file_1574830821.jpg', 'Kosong'),
(24, 'Ikan Bakar Rica', 'Ikan Bakar Rica + Nasi + Sayur', NULL, 30000, 1, 'Makanan', 1, 'file_1574831071.jpg', 'Tersedia'),
(25, 'Rahang Tuna', 'Rahang Tuna + Nasi + Sayur', NULL, 40000, 1, 'Makanan', 1, 'file_1574831654.jpg', 'Tersedia'),
(26, 'Ikan Bakar Rica Rica', 'Ikan Bakar Rica Rica + Nasi + Sayur', NULL, 30000, 1, 'Makanan', 1, 'file_1574831944.jpg', 'Kosong'),
(27, 'Ikan Gurame Bakar', 'Ikan Gurame Bakar + Nasi + Sayur', NULL, 30000, 1, 'Makanan', 1, 'file_1574832045.jpg', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pelanggan`
--

CREATE TABLE `tbl_pelanggan` (
  `plg_id` int(11) NOT NULL,
  `plg_nama` varchar(80) DEFAULT NULL,
  `plg_alamat` varchar(60) DEFAULT NULL,
  `plg_jenkel` varchar(2) DEFAULT NULL,
  `plg_notelp` varchar(30) DEFAULT NULL,
  `plg_email` varchar(40) DEFAULT NULL,
  `plg_photo` varchar(20) DEFAULT NULL,
  `plg_register` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `plg_password` varchar(35) DEFAULT NULL,
  `plg_status` int(11) DEFAULT '0',
  `kode` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pelanggan`
--

INSERT INTO `tbl_pelanggan` (`plg_id`, `plg_nama`, `plg_alamat`, `plg_jenkel`, `plg_notelp`, `plg_email`, `plg_photo`, `plg_register`, `plg_password`, `plg_status`, `kode`) VALUES
(8, 'Thomas Muller', 'Germany', 'L', '082169071552', 'email@gmail.com', 'user_blank.png', '2016-10-11 03:39:22', 'e10adc3949ba59abbe56e057f20f883e', 0, ''),
(9, 'Kevin De Bruyne', 'Belgia', 'L', '081277159401', 'email@gmail.com', 'user_blank.png', '2016-10-11 03:39:25', 'e10adc3949ba59abbe56e057f20f883e', 0, ''),
(25, 'charlie', 'kawangkoan', 'L', '123123123', 'charlie09@gmail.com', NULL, '2019-11-06 01:32:05', '123123', 0, ''),
(28, 'Meja001', 'Dabu Dabu Lemong', 'L', '001', 'Meja001', NULL, '2019-11-08 03:54:51', '123123', 1, ''),
(29, 'Meja002', 'Dabu Dabu Lemong', 'L', '002', 'Meja002', NULL, '2019-11-08 07:14:28', '123123', 0, ''),
(30, 'Meja003', 'Dabu Dabu Lemong', 'L', '003', 'Meja003', NULL, '2019-11-09 04:46:01', '123123', 1, ''),
(32, 'Meja004', 'Dabu Dabu Lemong', 'L', '004', '004', NULL, '2019-11-16 16:42:09', '123123', 1, ''),
(33, 'brenda', 'korea', 'L', '123123123', 'brenda@gmail.com', NULL, '2019-11-18 03:29:00', '123123', 0, ''),
(46, 'dev', 'manado', 'L', '123123', 'dev@gmail.com', NULL, '2019-11-24 02:19:43', '123123', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna`
--

CREATE TABLE `tbl_pengguna` (
  `pengguna_id` int(11) NOT NULL,
  `pengguna_nama` varchar(60) DEFAULT NULL,
  `pengguna_jenkel` varchar(2) DEFAULT NULL,
  `pengguna_username` varchar(30) DEFAULT NULL,
  `pengguna_password` varchar(35) DEFAULT NULL,
  `pengguna_email` varchar(50) DEFAULT NULL,
  `pengguna_nohp` varchar(30) DEFAULT NULL,
  `pengguna_status` int(2) DEFAULT '1',
  `pengguna_level` varchar(2) DEFAULT NULL,
  `pengguna_photo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pengguna`
--

INSERT INTO `tbl_pengguna` (`pengguna_id`, `pengguna_nama`, `pengguna_jenkel`, `pengguna_username`, `pengguna_password`, `pengguna_email`, `pengguna_nohp`, `pengguna_status`, `pengguna_level`, `pengguna_photo`) VALUES
(2, 'Charlie', 'L', 'admin', '123123', 'charlie@gmail.com', '081277159401', 1, '1', 'file_1573272965.jpg'),
(3, 'Cumi Bakar', 'L', 'apasaja11', 'a8ecfead8484d98505976f31bc6ebb7d', 'apasaja@gmail.com', '123456789', 1, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekening`
--

CREATE TABLE `tbl_rekening` (
  `rek_id` varchar(10) NOT NULL,
  `rek_no` varchar(60) DEFAULT NULL,
  `rek_nama` varchar(50) DEFAULT NULL,
  `rek_bank` varchar(30) DEFAULT NULL,
  `rek_cabang` varchar(50) DEFAULT NULL,
  `rek_logo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rekening`
--

INSERT INTO `tbl_rekening` (`rek_id`, `rek_no`, `rek_nama`, `rek_bank`, `rek_cabang`, `rek_logo`) VALUES
('001', '123456789', 'Charlie Tjiptomo', 'BCA', 'Manado', 'file_1482154688.png'),
('002', '987654321', 'Charlie Tjiptomo', 'BRI', 'Manado', 'file_1482156414.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `status_id` int(11) NOT NULL,
  `status_nama` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`status_id`, `status_nama`) VALUES
(1, 'Menunggu Konfirmasi'),
(2, 'Menunggu Pembayaran'),
(3, 'Pembayaran Selesai'),
(4, 'Dalam Pembuatan'),
(7, 'Transaksi Selesai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_detail`
--
ALTER TABLE `tbl_detail`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `detail_inv_no` (`detail_inv_no`),
  ADD KEY `detail_menu_id` (`detail_menu_id`);

--
-- Indexes for table `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  ADD PRIMARY KEY (`inv_no`),
  ADD KEY `inv_plg_id` (`inv_plg_id`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tbl_konfirmasi`
--
ALTER TABLE `tbl_konfirmasi`
  ADD PRIMARY KEY (`konfirmasi_id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `menu_kategori_id` (`menu_kategori_id`);

--
-- Indexes for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`plg_id`);

--
-- Indexes for table `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  ADD PRIMARY KEY (`pengguna_id`);

--
-- Indexes for table `tbl_rekening`
--
ALTER TABLE `tbl_rekening`
  ADD PRIMARY KEY (`rek_id`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_detail`
--
ALTER TABLE `tbl_detail`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_konfirmasi`
--
ALTER TABLE `tbl_konfirmasi`
  MODIFY `konfirmasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  MODIFY `plg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  MODIFY `pengguna_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detail`
--
ALTER TABLE `tbl_detail`
  ADD CONSTRAINT `tbl_detail_ibfk_1` FOREIGN KEY (`detail_inv_no`) REFERENCES `tbl_invoice` (`inv_no`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_detail_ibfk_2` FOREIGN KEY (`detail_menu_id`) REFERENCES `tbl_menu` (`menu_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_invoice`
--
ALTER TABLE `tbl_invoice`
  ADD CONSTRAINT `tbl_invoice_ibfk_1` FOREIGN KEY (`inv_plg_id`) REFERENCES `tbl_pelanggan` (`plg_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD CONSTRAINT `tbl_menu_ibfk_1` FOREIGN KEY (`menu_kategori_id`) REFERENCES `tbl_kategori` (`kategori_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
